import java.io.IOException;
import java.nio.file.Path;

public interface BigFileSorter {

    Path sort() throws IOException;

}
