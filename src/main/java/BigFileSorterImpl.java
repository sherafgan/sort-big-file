import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.BiFunction;

public class BigFileSorterImpl implements BigFileSorter {
    private final Path source;
    private final File target;
    private final Boolean ascending;
    private final Boolean sortLine;

    public BigFileSorterImpl(File source, File target, Boolean ascending, Boolean sortLine) {
        this.source = source.toPath();
        this.target = target;
        this.ascending = ascending;
        this.sortLine = sortLine;
    }

    @Override
    public Path sort() throws IOException {
        Path fileToBeSorted;
        if (sortLine) {
            fileToBeSorted = sortLineChars(source);
        } else {
            fileToBeSorted = Utils.copy(source);
        }
        return sortLines(fileToBeSorted);
    }

    private Path sortLines(Path fileToBeSorted) throws IOException {
        ProgressBar pb = new ProgressBarBuilder()
                .setTaskName("SORTING LINES (progress is in bytes count) ...")
                .setInitialMax(Files.size(fileToBeSorted))
                .setStyle(ProgressBarStyle.ASCII)
                .build();
        try (FileWriter fileWriter = new FileWriter(target)) {
            if (ascending) {
                writeInAscendingOrder(fileToBeSorted, fileWriter, pb);
            } else {
                writeInDescendingOrder(fileToBeSorted, fileWriter, pb);
            }
        }
        pb.stepTo(pb.getMax());
        pb.refresh();
        return target.toPath();
    }

    private void writeInDescendingOrder(Path fileToBeSorted, FileWriter fileWriter, ProgressBar pb) throws IOException {
        String lastMax = "";
        while (lastMax != null) {
            fileWriter.write(lastMax + "\n");
            pb.stepBy(lastMax.getBytes().length);
            try (
                    LineIterator it = FileUtils.lineIterator(fileToBeSorted.toFile(), "UTF-8");
            ) {
                String currentMax = "";
                while (it.hasNext()) {
                    String line = it.nextLine();
                    if (line.compareTo(currentMax) > 0 && line.compareTo(lastMax) < 0) {
                        currentMax = line;
                    }
                }
                lastMax = currentMax.isBlank() ? null : currentMax;
            }
        }
    }

    private void writeInAscendingOrder(Path fileToBeSorted, FileWriter fileWriter, ProgressBar pb) throws IOException {
        String lastMin = getLine(fileToBeSorted, new GetMinLineFunction());
        while (lastMin != null) {
            fileWriter.write(lastMin + "\n");
            pb.stepBy(lastMin.getBytes().length);
            try (
                    LineIterator it = FileUtils.lineIterator(fileToBeSorted.toFile(), "UTF-8");
            ) {
                String currentMin = getLine(fileToBeSorted, new GetMaxLineFunction());
                while (it.hasNext()) {
                    String line = it.nextLine();
                    if (line.compareTo(currentMin) < 0 && line.compareTo(lastMin) > 0) {
                        currentMin = line;
                    }
                }
                lastMin = currentMin.equals(lastMin) ? null : currentMin;
            }
        }
    }

    private String getLine(Path fileToBeSorted, BiFunction<String, String, String> lineFunc) throws IOException {
        String result;
        try (
                LineIterator it = FileUtils.lineIterator(fileToBeSorted.toFile(), "UTF-8");
        ) {
            result = it.hasNext() ? it.nextLine() : null;
            while (it.hasNext()) {
                String line = it.nextLine();
                result = lineFunc.apply(result, line);
            }
        }
        return result;
    }

    private Path sortLineChars(Path pathToFile) throws IOException {
        Path sortedWords = Files.createTempFile("eachLineSorted-", "-tmp.txt");
        sortedWords.toFile().deleteOnExit();
        try (
                LineIterator it = FileUtils.lineIterator(pathToFile.toFile(), "UTF-8");
                FileWriter writer = new FileWriter(sortedWords.toFile());
        ) {
            Iterator<String> progressableIterator = ProgressBar.wrap(it, "Sorting characters in each line ...");
            while (progressableIterator.hasNext()) {
                String line = progressableIterator.next();
                Character[] lineChars = ArrayUtils.toObject(line.toCharArray());
                if (ascending) {
                    Arrays.sort(lineChars);
                } else {
                    Arrays.sort(lineChars, Collections.reverseOrder());
                }
                line = joinChars(lineChars);
                writer.write(line + "\n");
            }
        }
        System.out.println("Temporary file with each lines' characters sorted -> " + sortedWords.toAbsolutePath() + "");
        return sortedWords;
    }

    private String joinChars(Character[] lineChars) {
        StringBuilder joined = new StringBuilder();
        for (Character lineChar : lineChars) {
            joined.append(lineChar);
        }
        return joined.toString();
    }

    static class GetMinLineFunction implements BiFunction<String, String, String> {
        @Override
        public String apply(String result, String line) {
            if (result != null && line.compareTo(result) < 0) {
                return line;
            } else {
                return result;
            }
        }
    }

    static class GetMaxLineFunction implements BiFunction<String, String, String> {
        @Override
        public String apply(String result, String line) {
            if (result != null && line.compareTo(result) > 0) {
                return line;
            } else {
                return result;
            }
        }
    }
}
