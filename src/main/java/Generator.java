import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Random;
import java.util.function.Function;

public class Generator implements Function<String, Path> {
    private final int lines;
    private final int maxLineLength;

    public Generator(int lines, int maxLineLength) {
        this.lines = lines;
        this.maxLineLength = maxLineLength;
    }

    @Override
    public Path apply(String filePath) {
        File file = new File(filePath);
        Random random = new Random();
        ProgressBarBuilder progressBarBuilder = new ProgressBarBuilder()
                .setTaskName("Generating random words file...")
                .setInitialMax(lines).setStyle(ProgressBarStyle.ASCII);
        try (
                FileWriter fileWriter = new FileWriter(file);
                ProgressBar pb = progressBarBuilder.build();
        ) {
            for (int i = 0; i < lines; i++) {
                String word = RandomStringUtils.randomAlphanumeric(random.nextInt(7, maxLineLength));
                fileWriter.write(word + "\n");
                pb.stepTo(i + 1);
            }
            return file.toPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file.toPath();
    }
}
