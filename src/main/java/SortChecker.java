import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;

public class SortChecker implements Function<Path, Boolean> {
    private final boolean ascending;
    private final boolean charsSorted;

    public SortChecker(boolean ascending, boolean eachLineSorted) {
        this.ascending = ascending;
        this.charsSorted = eachLineSorted;
    }

    @Override
    public Boolean apply(Path path) {
        ProgressBar pb = createProgressBar(path);
        try (
                LineIterator it = FileUtils.lineIterator(path.toFile(), "UTF-8");
        ) {
            String prev = it.hasNext() ? it.nextLine() : null;
            if (charsSorted && prev != null && !isSorted(prev, ascending)) return false;
            while (it.hasNext()) {
                String line = it.nextLine();
                if (charsSorted && !isSorted(line, ascending)) return false;
                if (prev != null &&
                        ((ascending && prev.compareTo(line) > 0) || (!ascending && prev.compareTo(line) < 0))
                ) {
                    return false;
                }
                prev = line;
                pb.stepBy(line.getBytes().length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        pb.stepTo(pb.getMax());
        pb.refresh();
        return true;
    }

    private boolean isSorted(String target, boolean ascending) {
        if (target.length() <= 1) return true;
        Character prev = target.charAt(0);
        for (int i = 1; i < target.length(); i++) {
            int comparison = prev.compareTo(target.charAt(i));
            if ((ascending && comparison > 0) || (!ascending && comparison < 0)) {
                return false;
            }
            prev = target.charAt(i);
        }
        return true;
    }

    private ProgressBar createProgressBar(Path path) {
        ProgressBarBuilder pbBuilder = new ProgressBarBuilder()
                .setTaskName("CHECKING THE SORTING ...");
        try {
            pbBuilder.setInitialMax(Files.size(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        pbBuilder.setStyle(ProgressBarStyle.ASCII)
                .build();
        return pbBuilder.build();
    }
}
