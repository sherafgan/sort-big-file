import picocli.CommandLine;

import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.function.Function;

@CommandLine.Command(
        name = "gen",
        description = "Generates random (big) text file w/ defined max line size."
)
public class BigGen implements Callable<Integer> {
    @CommandLine.Option(names = "-nol", description = "Number of lines to be generated. Default - 100.")
    int numberOfLines = 100;

    @CommandLine.Option(names = "-mll", description = "Maximum line length. Default - 21.")
    int maxLineLength = 21;

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;

    @Override
    public Integer call() throws Exception {
        String path = System.getProperty("user.dir") + "/gen-" + UUID.randomUUID() + ".txt";
        Function<String, Path> generator = new Generator(numberOfLines, maxLineLength);
        Path result = generator.apply(path);
        System.out.println("GENERATED to -> " + result.toAbsolutePath());
        return 0;
    }
}
