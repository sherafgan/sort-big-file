import com.google.code.externalsorting.ExternalSort;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "sort",
        description = "Sorts the given file and prints the path to sorted file."
)
public class BigSort implements Callable<Integer> {
    @CommandLine.Parameters(index = "0", description = "The file to be sorted by the program.")
    private File file;

    @CommandLine.Option(
            names = {"-a", "--algorithm"},
            defaultValue = "LIM",
            description = "Valid values: ${COMPLETION-CANDIDATES}. Default - LIM."
    )
    private Algorithm algorithm;

    @CommandLine.Option(names = "-desc", description = "desc - sort in descending order. Default: ascending order.")
    private boolean desc;

    @CommandLine.Option(names = "-el", description = "el - Sort characters in each line too. " +
            "Applicable for LIM algorithm only.")
    private boolean el;

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;

    @Override
    public Integer call() throws Exception {
        File target = new File(System.getProperty("user.dir") + "/sorted-" + file.getName());
        Path result = sort(file, target, algorithm);
        System.out.println("SORTED to -> " + result.toAbsolutePath());
        return 0;
    }

    public Path sort(File source, File target, Algorithm algorithm) throws IOException {
        switch (algorithm) {
            case EXT:
                ExternalSort.mergeSortedFiles(ExternalSort.sortInBatch(source), target);
                return target.toPath();
            case LIM:
            default:
                BigFileSorter sorter = new BigFileSorterImpl(file, target, !desc, el);
                return sorter.sort();
        }
    }

    enum Algorithm {
        /**
         * LIM - sorts sequentially big text file. Skips duplicates of already sorted lines.
         */
        LIM,
        /**
         * EXT - fast 3rd party multi-core external merge-sort implementation.
         */
        EXT
    }
}
