import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class Utils {
    public static Path copy(Path originalPath) throws IOException {
        Path copied = Files.createTempFile("bigFileSorted-", ".tmp");
        copied.toFile().deleteOnExit();
        Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
        return copied;
    }
}
