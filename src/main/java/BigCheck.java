import picocli.CommandLine;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.function.Function;

@CommandLine.Command(
        name = "check",
        description = "Checks whether (big) text file's is sorted or not."
)
public class BigCheck implements Callable<Integer> {
    @CommandLine.Parameters(index = "0", description = "The file to be sorted by the program.")
    private File file;

    @CommandLine.Option(names = "-desc", description = "desc - sorted in descending order. Default: ascending order.")
    private boolean desc;

    @CommandLine.Option(
            names = "-el",
            description = "el - Should I check whether each line's characters are sorted or not. Default: do not check."
    )
    private boolean el;

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;

    @Override
    public Integer call() throws Exception {
        Function<Path, Boolean> checker = new SortChecker(!desc, el);
        String result = checker.apply(file.toPath()) ? "[----- YES! -----]" : "[----- NO! -----]";
        System.out.println("SORTED? -> " + result);
        return 0;
    }
}