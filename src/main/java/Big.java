import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "big",
        mixinStandardHelpOptions = true,
        version = "big 1.0",
        description = "Generates, sorts, and checks big files of text.",
        subcommands = {BigSort.class, BigGen.class, BigCheck.class}
)
public class Big implements Callable<Integer> {

    @CommandLine.Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;

    public static void main(String[] args) {
        CommandLine cmd = new CommandLine(new Big());
        cmd.setExecutionStrategy(new CommandLine.RunAll());
        cmd.execute(args);

        if (args.length == 0) {
            cmd.usage(System.out);
        }
    }

    @Override
    public Integer call() throws Exception {
        return -1;
    }
}
