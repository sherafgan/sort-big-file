import org.apache.commons.io.FileUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigFileSorterImplTest {
    @ParameterizedTest
    @CsvSource(value = {
            "random/ten_lines.txt, random/ten_lines_sorted.txt, true, false",
            "random/ten_lines.txt, random/ten_lines_sorted_desc.txt, false, false",
            "duplicates/duplicates.txt, duplicates/duplicates_sorted.txt, true, false",
            "sort_each_line/two_lines.txt, sort_each_line/two_lines_sorted.txt, true, true",
            "sort_each_line/two_lines.txt, sort_each_line/two_lines_sorted_desc.txt, false, true"
    })
    void test_bigFileSorterResults_ok(String unsorted, String sorted, boolean asc, boolean eachLineSorted) throws IOException {
        File source = FileUtils.getFile(Objects.requireNonNull(getClass().getResource(unsorted)).getPath());
        File target = new File(System.getProperty("user.dir") + "/sorted-" + source.getName());
        BigFileSorter bigFileSorter = new BigFileSorterImpl(source, target, asc, eachLineSorted);
        File result = bigFileSorter.sort().toFile();
        result.deleteOnExit();
        FileUtils.contentEquals(
                result,
                Path.of(Objects.requireNonNull(getClass().getResource(sorted)).getPath()).toFile()
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
            "random/ten_lines_sorted.txt, true, false, true",
            "random/ten_lines_sorted_desc.txt, false, false, true",
            "sort_each_line/two_lines_sorted.txt, true, true, true",
            "sort_each_line/two_lines_sorted_desc.txt, false, true, true",
            "not_sorted/two_lines_sorted_w_error.txt, true, true, false",
            "not_sorted/two_lines_sorted_w_error_desc.txt, false, true, false",
    })
    void test_isSortedAlgo_ok(String sorted, boolean asc, boolean eachLineSorted, boolean expected) throws IOException {
        Path target = Path.of(Objects.requireNonNull(getClass().getResource(sorted)).getPath());
        Function<Path, Boolean> checker = new SortChecker(asc, eachLineSorted);
        assertEquals(expected, checker.apply(target));
    }
}