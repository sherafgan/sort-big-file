# sort-big-file (CLI)

Simple CLI command for generating, sorting _(lexicographically)_, and checking big (not fitting into RAM) text files.

Currently the ```sort``` command under the ```-a``` option has two sorting algorithms. ```LIM``` (i.e. limited) and ```EXT``` (i.e. external)).
1. LIM -  sorts sequentially big text file. Skips duplicates of already sorted lines.

It works only for files without duplicate lines, if a file having multiple identical lines is passed into the sort
command, the ```LIM``` algorithm only writes the first encountered line into the final sorted file.
Also the ```LIM``` algorithm accepts ```-el``` (i.e. each line) option, in this case the algorithm will also sort each line too (meaning characters in each line).

Complexity: Time O(n^2), Space O(1). In case of passing ```-el``` option to the sorting algorithm, the time complexity
will be O(n^2 * K), where K - is the average length of lines in the given file.

```bash
  -a, --algorithm=<algorithm>
               Valid values: LIM, EXT. Default - LIM.
      -desc    desc - sort in descending order. Default: ascending order.
      -el      el - Sort characters in each line too. Applicable for LIM
                 algorithm only.
  -h, --help   display a help message
```
2. EXT - fast 3rd party multi-core external merge-sort implementation.

### Prerequisites

* Java 17 or later ([MacOS](https://formulae.brew.sh/formula/openjdk)
  / [Linux](https://techviewleo.com/install-java-openjdk-on-ubuntu-linux/)
  / [Windows](https://docs.oracle.com/en/java/javase/17/install/installation-jdk-microsoft-windows-platforms.html#GUID-A7E27B90-A28D-4237-9383-A58B416071CA))

### Steps

* Build an executable (i.e. fat jar).
* Run it. Done! : )

### Building

```bash
cd sort-big-file # go to project directory you cloned
./gradlew fatJar # run gradle task to build a fat jar (executable)
```

After executing the commands above you'll have an artifact alike ```./build/libs/sort-big-file-1.0-SNAPSHOT-standalone.jar```.

### Running

Now you can run it and see the available commands and options using ```-h```. This program has a help manual like any
other CLI command, so you can find the necessary commands and flags using the ```-h``` option as you do with other bash commands.

```bash
java -jar build/libs/sort-big-file-1.0-SNAPSHOT-standalone.jar -h
```

Output:

```console
Usage: big [-h] [COMMAND]
Generates, sorts, and checks big files of text.
  -h, --help   display a help message
Commands:
  sort   Sorts the given file and prints the path to sorted file.
  gen    Generates random (big) text file w/ defined max line size.
  check  Checks whether (big) text file's is sorted or not.
```

### Usage scenario

1. Generate random text file with 20_000 lines and max line length of 21.

```bash
java -jar build/libs/sort-big-file-1.0-SNAPSHOT-standalone.jar gen -mll 21 -nol 20000 

# OUTPUT
Generating random words file... 100% [======================================================================================] 20000/20000 (0:00:00 / 0:00:00)
GENERATED to -> /Users/sher/IntelliJ-Projects/sort-big-file/gen-f9d5b74c-366a-435d-b848-003fe201b3b0.txt
```

2. Sort the generated file.

```bash
java -jar build/libs/sort-big-file-1.0-SNAPSHOT-standalone.jar sort gen-f9d5b74c-366a-435d-b848-003fe201b3b0.txt

# OUTPUT
SORTING LINES (progress is in bytes count) ... 100% [=====================================================================] 290586/290586 (0:00:53 / 0:00:00)
SORTED to -> /Users/sher/IntelliJ-Projects/sort-big-file/sorted-gen-f9d5b74c-366a-435d-b848-003fe201b3b0.txt
```

3. Check result of sorting.

```bash
java -jar build/libs/sort-big-file-1.0-SNAPSHOT-standalone.jar check sorted-gen-f9d5b74c-366a-435d-b848-003fe201b3b0.txt

#OUTPUT
CHECKING THE SORTING ...   0% [>                                                                                                ]      0/290586 (0:00:00 / ?)
CHECKING THE SORTING ... 100% [===========================================================================================] 290586/290586 (0:00:00 / 0:00:00)
SORTED? -> [----- YES! -----]
```
